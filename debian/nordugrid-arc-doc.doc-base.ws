Document: arc-ws-quick
Title: ARC Web Services - Quick Guide
Author: NorduGrid developers
Abstract: Instructions to setup a Grid site using ARC.
Section: Network/Communication

Format: PDF
Files: /usr/share/doc/nordugrid-arc-doc/ws-quick-guide.pdf
